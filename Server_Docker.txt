Wenn Jira down ist:

* per Bash/Shell/Putty �ber ssh auf Server verbinden: 

--> ssh dolly@31.172.93.251

* Folgendes Kommando zeigt alle laufenden Container: 

--> docker ps

* Wenn kein Container mit Name "jira"  oder Image "cptactionhank/atlassian-jira" zu sehen ist:

--> docker restart a557def 

* Kurz warten als Best�tigung erscheint in einer neuen Zeile "a557def" und erneut pr�fen mit

--> docker ps 

* Der Container m�sste jetzt sichtbar sein, es dauert ca 1-2 Minuten bis Jira vollst�ndig hochgefahren ist